"""
co2-ampel
===========
This app shows the current equivalent CO2 value and displays a led warning if above certain thresholds.
"""
import buttons
import color
import display
import os
import time
import bme680
import leds
import color


def main():
    disp = display.open()
    disp.clear().update()
    leds.clear()
    with bme680.Bme680() as environment:
        while True:
            data = environment.get_data()
            if data.eco2 < 1000:
                c=color.GREEN
            elif data.eco2 < 2000:
                c=color.YELLOW
            else:
                c=color.RED
            disp.clear()
            disp.print("CO2 (ppm)", posx=0, posy=0, font=1)
            disp.print("{:7.2f}".format(data.eco2), posx=10, posy=35, font=4, fg=c)
            disp.update()
            leds.set(leds.BOTTOM_LEFT, c)
            time.sleep(3)


if __name__ == "__main__":
    main()
